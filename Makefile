include env.mk

.PHONY: help build clean deploy checks checks-fix nag-ci nag npm-requirements requirements test-requirements package test-unit remove-stack ssm-initialise
all help: ## show this help message
	@F=': .*\#\# '; awk -F "$$F" "/$$F/"' {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) 2>/dev/null

build: npm-requirements ## compile CDK code
	mkdir -p build # this enables cdk building without lambda packaging
	npm run build

clean: ## remove generated files
	rm -rf build
	rm -rf .cache asset.* build TEST-*-results.xml
	find . -name '*.pyc' -delete

deploy: ## Deploy the stack to AWS environment
	npm run deploy

checks: test-requirements ## run eslint, flake8, safety, bandit
	flake8 app/
	safety check -r app/requirements/base.txt
	bandit -r app -v -f xml -o bandit.xml -x app/tests
	npm run eslint

checks-fix: requirements-test ## auto-fix quality check issues
	npm run lint-fix
	autopep8 --in-place --recursive -aa app/

nag-ci: ## runs a continuous integration specific version of cloudformation nag
	for FILE in `find . -type f -wholename './$(PPLAWS_PRODUCT)-*.template.json'`; do cfn_nag_scan --input-path $${FILE}; done

nag: ## runs a local friendly version of cloudformation nag
	for FILE in `find . -type f -wholename './$(PPLAWS_PRODUCT)-*.template.json'`; do docker run -v ${PWD}:/templates -t stelligent/cfn_nag /templates/$${FILE}; done

npm-requirements: ## installs npm requirements for cdk
	npm ci

remove-stack: ## remove CF stack
	npm run remove-stack $(STACK_NAME)

requirements: npm-requirements ## installs python application dependencies
	pip3 install -r app/requirements/base.txt --index-url='https://nexus3.tl.pplaws.com/repository/pypi-all/simple'

package: ## packages dependencies with source for deployable code artifact
	rm -rf build
	cp -r app build
	pip3 install -r app/requirements/base.txt -t build --index-url='https://nexus3.tl.pplaws.com/repository/pypi-all/simple'

ssm-initialise: npm-requirements ## initialise SSM Parameters
	npm run ssm-initialise

test-requirements: ## installs python dependencies for running tests
	pip3 install -r app/requirements/test.txt --index-url='https://nexus3.tl.pplaws.com/repository/pypi-all/simple'

test-unit: test-requirements ## runs unit tests
	pytest --cov=./app/src --cov-fail-under 70 --cov-report term-missing --cov-report xml app/tests/unit/ -vv --junitxml=TEST-unit-results.xml
