from importlib import reload

from pytest import fixture
from src.lambda_function import app


@fixture(autouse=True)
def env_vars_injection(monkeypatch):
    monkeypatch.setenv("AWS_XRAY_SDK_ENABLED", "false")
    reload(app)


def test_lambda_handler():
    app.lambda_handler(None, None)
