import boto3
import json
import os
from ppl_json_logger import LoggingClient

LOG = LoggingClient()
LAMBDA_CLIENT = boto3.client("lambda")
NUM_LAMBDAS = int(os.getenv("NUM_LAMBDAS"))
NUM_CONTACT_MOMENTS = int(os.getenv("NUM_CONTACT_MOMENTS"))
DSAP = os.getenv("DSAP") or "at-jtb"


def invoke_lambda_function():
    return LAMBDA_CLIENT.invoke(
        FunctionName=f'bip-servicemix-payments-test-{DSAP}',
        InvocationType='Event',
        Payload=json.dumps({
            "numContactMoments": NUM_CONTACT_MOMENTS
        }).encode(),
        Qualifier="at-jtb"
    )


def main():
    for _ in range(NUM_LAMBDAS):
        invoke_lambda_function()


if __name__ == "__main__":
    main()
