import concurrent.futures
import os
from random import randint
from ppl_json_logger import LoggingClient
from ppl_legacy_servicemix_client import ServiceMixMessageDispatcher
from ppl_payments_common import Field, Status, Type

SCRIPT_DIR = os.path.dirname(__file__)
LOG = LoggingClient()
UTF_8 = "utf-8"

MAX_WORKERS = int(os.getenv("MAX_WORKERS"))

STATUSES = [
    Status.SUCCESS.value,
    Status.DECLINED.value,
    Status.FAILED.value
]

EVENT_TYPES = [
    Type.PAYMENT.value,
    Type.PAYOUT.value
]

with open(os.path.join(SCRIPT_DIR, "resources/contact-moment-for-payment-response.xml")) as f:
    CONTACT_MOMENT_FOR_PAYMENT_RESPONSE_TEMPLATE = f.read()

SERVICEMIX_DISPATCHER = ServiceMixMessageDispatcher(
    active_mq_queue_name='CommunicationManager.contact.new',
    logging_client=LOG
)


def lambda_handler(event, context):
    NUM_CONTACT_MOMENTS = event["numContactMoments"]
    with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        _ = {
            executor.submit(
                send_contact_moment_message,
                generate_test_payment_data(),
                i
            ) for i in range(NUM_CONTACT_MOMENTS)
        }


def send_contact_moment_message(payment_event: dict, index: int):
    contact_moment_message = generate_contact_moment_message(
        customer_id=payment_event[Field.CUSTOMER_ID.json_key],
        event_type=payment_event[Field.EVENT_TYPE.json_key],
        event_message=payment_event[Field.MESSAGE.json_key]
    )
    LOG.info(f"Generated contact moment message for payment: {index}", {
        Field.CUSTOMER_ID.json_key: payment_event[Field.CUSTOMER_ID.json_key]
    })
    SERVICEMIX_DISPATCHER.send_message_to_servicemix(contact_moment_message)


def generate_contact_moment_message(
    customer_id: str,
    event_type: str,
    event_message: str
) -> bytes:
    LOG.info("Generating contact moment message using template and provided values")
    return CONTACT_MOMENT_FOR_PAYMENT_RESPONSE_TEMPLATE.format(
        customer_id=customer_id,
        event_type=event_type,
        event_message=event_message
    ).encode(UTF_8)


def generate_test_payment_data() -> dict:
    event_type = EVENT_TYPES[randint(0, len(EVENT_TYPES) - 1)]
    return {
        Field.CUSTOMER_ID.json_key: randint(10000000, 10000511),
        Field.EVENT_TYPE.json_key: event_type,
        Field.MESSAGE.json_key: f"{event_type} attempted on somedate has status {STATUSES[randint(0, len(STATUSES) - 1)]} - Part of load test by BIP Payments"
    }
