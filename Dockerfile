FROM 861698843696.dkr.ecr.eu-west-1.amazonaws.com/build-aws:latest

COPY . /

WORKDIR /

RUN npm ci

USER root

RUN pip3 install -r app/requirements/test.txt --trusted-host pypi.python.org --index-url https://nexus3.tl.pplaws.com/repository/pypi-all/simple

USER jenkins
