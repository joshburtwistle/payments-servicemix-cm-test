import {
  Construct, Duration, Stack, StackProps,
} from '@aws-cdk/core';
import {
  Alias, Code, Function as LambdaFunction, Runtime, Tracing,
} from '@aws-cdk/aws-lambda';
import { LambdaDeploymentConfig, LambdaDeploymentGroup } from '@aws-cdk/aws-codedeploy';
import { Topic } from '@aws-cdk/aws-sns';
import PPLVPC from '@ppl-cdk/ppl-vpc';
import CloudWatchLogGroupHandler from '@ppl-cdk/cloudwatch-log-group-handler';
import { PolicyStatement } from '@aws-cdk/aws-iam';

export default class BipServicemixPaymentsTestStack extends Stack {
  public constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const awsConfig = `${Stack.of(this).region}:${Stack.of(this).account}`;
    const { vpc } = new PPLVPC(this, 'PplVpc');

    const environment: string = process.env.ENVIRONMENT || 'st';
    const deploymentConfig = (['at', 'pr'].includes(environment))
      ? LambdaDeploymentConfig.LINEAR_10PERCENT_EVERY_1MINUTE
      : LambdaDeploymentConfig.ALL_AT_ONCE;
    const nowAsISOString = new Date().toISOString();

    const functionName = `bip-servicemix-payments-test-${environment}`;

    const lambda = new LambdaFunction(this, 'Lambda', {
      functionName,
      runtime: Runtime.PYTHON_3_7,
      timeout: Duration.minutes(15),
      memorySize: 10240,
      code: Code.fromAsset('./build'),
      handler: 'src.lambda_function.app.lambda_handler',
      description: `${functionName} Lambda - Generated at ${nowAsISOString}.`,
      vpc,
      vpcSubnets: {
        subnetGroupName: 'private'
      },
      environment: {
        MAX_WORKERS: "10"
      }
    });

    const topic = new Topic(this, 'Topic', {
      topicName: `bip-servicemix-payments-test-topic-${environment}`,
    });

    const lambdaAlias = new Alias(this, 'LambdaAlias', {
      aliasName: environment,
      version: lambda.currentVersion,
    });

    lambdaAlias.addToRolePolicy(new PolicyStatement({
      resources: [
        `arn:aws:ssm:${awsConfig}:parameter/beehive/servicemix/activemq/username`,
        `arn:aws:ssm:${awsConfig}:parameter/beehive/servicemix/activemq/password`,
      ],
      actions: ['ssm:GetParameters'],
    }))

    const { logGroup } = new CloudWatchLogGroupHandler(this, 'LambdaCloudWatchLogGroupHandler', {
      logGroupName: `/aws/lambda/${functionName}`,
      retentionInDays: 90,
    });

    new LambdaDeploymentGroup(this, 'LambdaBlueGreenDeployment', {
      alias: lambdaAlias,
      deploymentConfig
    });
  }
}
