module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "extends": [
        "airbnb-base",
        "plugin:@typescript-eslint/recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "plugins": [
        "@typescript-eslint"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "indent": "off",
        "no-new": 0,
        "@typescript-eslint/indent": [
            "error",
            2
        ],
        "import/extensions":[
            "error",
            "ignorePackages",
            {
                "js": "never",
                "jsx": "never",
                "ts": "never",
                "tsx": "never"
            }
        ],
    },
    "overrides": [
        {
            "files": [
                "**/__mocks__/**",
                "**/__tests__/**",
                "**/*.test.ts",
                "**/*.spec.ts"
            ],
            "env": {
                "jest": true
            },
            "rules": {
                "import/no-extraneous-dependencies": ['error', {devDependencies: true}]
            }
        }
    ],
    "settings": {
        "parser": "typescript-eslint-parser",
        "plugins": [
            "import"
        ],
        "rules": {
            // turn on errors for missing imports
            "import/no-unresolved": "error"
        },
        "import/resolver": {
            // use <root>/tsconfig.json
            "typescript": {}
        }
    }
};
