#!/usr/bin/env node
import 'source-map-support/register';
import { App } from '@aws-cdk/core';
import { applyAllPrefixedTagsFromEnv } from '@ppl-cdk/tags';
import BipServicemixPaymentsTestStack from '../lib/bip-servicemix-payments-test-stack';

const app = new App();
const stackName = process.env.STACK_NAME || 'bip-servicemix-payments-test';

applyAllPrefixedTagsFromEnv(new BipServicemixPaymentsTestStack(app, stackName));
