#!/usr/bin/env bash

wget -N http://jenkins.tl.pplaws.com/jnlpJars/jenkins-cli.jar

STATUS=1
PIPELINE_TEMPLATE_RESULT=$(java -jar jenkins-cli.jar -s https://jenkins.tl.pplaws.com/ declarative-linter < groovy/pipeline.groovy )
JENKINS_SUCCESS_MESSAGE='Jenkinsfile successfully validated.'

if [[ ${PIPELINE_TEMPLATE_RESULT}  == ${JENKINS_SUCCESS_MESSAGE} ]]; then
  echo -e "\nSuccessful validated Pipeline Template Jenkinsfile.\n"
  STATUS=0
else
  echo -e "\nLinting failed for Pipeline Template JenkinsFile: \n" ${PIPELINE_TEMPLATE_RESULT}
fi

exit ${STATUS}
