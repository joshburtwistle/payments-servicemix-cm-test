def isMain() {
  return (env.BRANCH_NAME == 'main' || env.BRANCH_NAME == 'origin/main')
}

def isProduction() {
  return env.PPLAWS_ENV == 'pr'
}

def getCommitLink() {
  return "<a href='${env.GIT_URL}/commits/${env.GIT_COMMIT}'>${env.GIT_COMMIT}</a>"
}

def isUpdatedCodebase() {
  env.GIT_COMMIT != env.GIT_PREVIOUS_SUCCESSFUL_COMMIT
}

def isStartedByTimer() {
  def buildCauses = currentBuild.rawBuild.getCauses()
  return buildCauses.contains("hudson.triggers.TimerTrigger\$TimerTriggerCause")
}

def notifyFailureOnMain() {
  if (isMain()) {
      office365ConnectorSend message: "Build Has Failed - ${getCommitLink()}", status:"FAILURE", color: "FF0000", webhookUrl:'https://'
  }
}

def notifySuccessOnMain() {
  if (isMain()) {
    office365ConnectorSend message: "Build Has Succeeded - ${getCommitLink()}", status:"SUCCESS", color: "008000", webhookUrl: 'https://'
  }
}

def notifyAbortOnMain() {
  if (isMain()) {
    office365ConnectorSend message: "Build Has Been Aborted - ${getCommitLink()}", status:"FAILURE", color: "808080", webhookUrl:'https://'
  }
}

// IMPORTANT - DO NOT REMOVE - this will cause the import/load call to the groovy file to return a null object
// error looks like java.lang.NullPointerException: Cannot invoke method foobar() on null object
return this
