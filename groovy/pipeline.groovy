def call(Map pipelineParams) {
    pipeline {
        // allocating the pipeline agent to "any" appears important to correctly inject checkout variables.
        agent any
        environment {
            PPLAWS_ENV = "${pipelineParams.account}"
            AWS_ACCOUNT_ID = "${pipelineParams.accountId}"
            TAG_Delete = "${pipelineParams.delete}"
            PROMOTE_TO_NEXT_ENVIRONMENT = "${pipelineParams.promoteToNextEnvironment}"
            SKIP_VERIFICATION = "${pipelineParams.skipVerification}"
        }

        options {
            timestamps()
            disableConcurrentBuilds()
            withCredentials(pipelineParams.credentials)
            parallelsAlwaysFailFast()
        }

        stages {
            stage('Agent Startup') {
                agent {
                    dockerfile {
                        filename 'Dockerfile'
                        reuseNode true
                        args '-v /home/ec2-user/tools/:/home/ec2-user/tools/'
                    }
                }
                stages {
                    stage('Environment') {
                        steps {
                            script {
                                common = load("groovy/common.groovy")
                            }
                        }
                    }
                    stage('Checks') {
                        steps {
                            sh 'make checks'
                        }
                    }
                    stage('Unit Test') {
                        steps {
                            sh 'make test-unit'
                        }
                        post {
                            always {
                                junit "TEST-unit-results.xml"
                            }
                        }
                    }
                    stage('Package') {
                        steps {
                            sh 'make package'
                        }
                    }
                    stage('Coverage') {
                        steps {
                            cobertura coberturaReportFile: 'coverage.xml'
                        }
                    }
                    stage('CDK Build & Nag') {
                        steps {
                            sh 'make build nag-ci'
                        }
                    }
                    stage('SonarCloud Quality Gate') {
                        when {
                            expression { env.SKIP_VERIFICATION != "yes" }
                        }
                        steps {
                            sonarcloud_quality_gate()
                        }
                    }
                    stage('Deploy') {
                        steps {
                            sh 'make deploy'
                        }
                    }
                    /*  TODO remove comment when integration tests are added
                    stage('Integration Tests') {
                        when {
                            expression { env.SKIP_VERIFICATION != "yes" }
                        }
                        steps {
                            sh 'make test-int'
                        }
                        post {
                            always {
                                junit "TEST-int-results.xml"
                            }
                        }
                    }
                     */
                    stage('PROMOTE TO NEXT') {
                        when {
                            expression { env.PROMOTE_TO_NEXT_ENVIRONMENT.trim() }
                        }
                        steps {
                            script {
                                try {
                                    build job: "${env.PROMOTE_TO_NEXT_ENVIRONMENT}", wait: false
                                }
                                catch (Exception ex) {
                                    println("Failed to schedule promotion to next environment: ${ex}")
                                }
                            }
                        }
                    }
                }
            }
        }
        post {
            failure {
                script { common.notifyFailureOnMain() }
            }
            fixed {
                script { common.notifySuccessOnMain() }
            }
            aborted {
                script { common.notifyAbortOnMain() }
            }
            cleanup {
                cleanWs()
            }
        }
    }
}

// IMPORTANT - DO NOT REMOVE - this will cause the import/load call to the groovy file to return a null object
// error looks like java.lang.NullPointerException: Cannot invoke method foobar() on null object
return this
