export AWS_DEFAULT_REGION?=eu-west-1
export AWS_REGION?=$(AWS_DEFAULT_REGION)
export AWS_ACCOUNT_ID ?= 107538011926

export REPO_NAME?=bip-servicemix-payments-test
export BRANCH_NAME?=$(shell git rev-parse --abbrev-ref HEAD)

export PPLAWS_TEAM?=BIPPayments
export PPLAWS_PRODUCT?=bip
export PPLAWS_CONTACT?=$(PPLAWS_TEAM)@postcodelottery.co.uk
export PPLAWS_GIT_REPOSITORY?=https://bitbucket.org/pplcs/$(REPO_NAME)
export PPLAWS_GIT_REF?=$(GIT_COMMIT)
export PPLAWS_MODULE?=$(REPO_NAME)
export PPLAWS_ENV?=st

export TAG_Product?=Improved Beehive
export TAG_Module?=Main
export TAG_Component?=Bip Servicemix Payments Test
export TAG_Contact?=$(PPLAWS_CONTACT)
export TAG_Team?=BIPPayments
export TAG_GitRepository?=$(PPLAWS_GIT_REPOSITORY)
export TAG_GitRef?=$(PPLAWS_GIT_REF)

export TAG_Delete?=daily

## replicating logic of ppl-cdk-names package for stack name
export STACK_NAME?=$(shell if [ '' != '$(PPLAWS_REFERENCE)' ]; then echo $(PPLAWS_PRODUCT)-$(PPLAWS_MODULE)-$(PPLAWS_ENV)-$(PPLAWS_REFERENCE); else echo $(PPLAWS_PRODUCT)-$(PPLAWS_MODULE)-$(PPLAWS_ENV) ; fi )
